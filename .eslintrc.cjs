/** @type {import("eslint").ESLint.ConfigData} */
const config = {
	extends: ["@prosomo/eslint-config"],
};

module.exports = config;
