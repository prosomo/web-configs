module.exports = [
	"last 3 chrome versions",
	"last 3 firefox versions",
	"last 3 opera versions",
	"last 3 edge versions",
	"last 3 safari versions",
	// Mobile browsers
	"last 3 chromeandroid versions",
	"last 1 firefoxandroid versions",
	"ios >= 14.0",
];
