# @prosomo/browserslist-config

## 2.0.0

### Major Changes

- b4adb1a: Drop support for iOS 13

## 1.0.1

### Patch Changes

- 7ee2058: Update dependencies

## 1.0.0

### Major Changes

- d454cd8: Initial release
