# `@prosomo/browserslist-config`

[![License](https://img.shields.io/gitlab/license/prosomo/web-configs?style=flat-square)](../../LICENSE)
![npm version](https://img.shields.io/npm/v/%40prosomo%2Fbrowserslist-config?style=flat-square)

Shared [browserslist](https://github.com/browserslist/browserslist) configuration for Prosomo.

## Supported Browsers

| Browser                     | Version |
| --------------------------- | ------- |
| Chrome & Chrome for Android | last 3  |
| Firefox                     | last 3  |
| Opera                       | last 3  |
| Edge                        | last 3  |
| Safari                      | last 3  |
| iOS                         | >= 13.4 |

You can list all supported browsers by running `npx browserslist "last 1 firefoxandroid versions, last 3 chrome versions, last 3 chromeandroid versions, last 3 firefox versions, last 3 opera versions, last 3 edge versions, last 3 safari versions, ios >= 13.4"`

## Installation

**With pnpm**

```bash
pnpm add --dev @prosomo/browserslist-config
```

**With Yarn**

```bash
yarn add --dev @prosomo/browserslist-config
```

**With npm**

```bash
npm install --save-dev @prosomo/browserslist-config
```

## Usage

Add a `browserslist` property in your `package.json`. See the [browserslist configuration docs](https://github.com/browserslist/browserslist#config-file) for more details.

```json
{
	"browserslist": ["extends @prosomo/browserslist-config"]
}
```
