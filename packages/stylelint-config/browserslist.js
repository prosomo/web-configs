/** @type {import("stylelint").Config} */
module.exports = {
	extends: ["./index.js"],
	plugins: ["stylelint-no-unsupported-browser-features"],
	rules: {
		"plugin/no-unsupported-browser-features": [true, { severity: "warning" }],
	},
};
