# `@prosomo/stylelint-config`

[![License](https://img.shields.io/gitlab/license/prosomo/web-configs?style=flat-square)](../../LICENSE)
![npm version](https://img.shields.io/npm/v/%40prosomo%2Fstylelint-config?style=flat-square)

Shared [stylelint](https://stylelint.io) configurations for Prosomo.

## Installation

**With pnpm**

```bash
pnpm add --dev stylelint stylelint-config-prettier stylelint-config-standard @prosomo/stylelint-config
```

**With Yarn**

```bash
yarn add --dev stylelint stylelint-config-prettier stylelint-config-standard @prosomo/stylelint-config
```

**With npm**

```bash
npm install --save-dev stylelint stylelint-config-prettier stylelint-config-standard @prosomo/stylelint-config
```

If you want browserslist support, also install `stylelint-no-unsupported-browser-features` and our [browserslist config](https://gitlab.com/prosomo/web-configs/blob/main/packages/browserslist-config/README.md).

## Available configs

- `@prosomo/stylelint-config`
- `@prosomo/stylelint-config/browserslist`

## Usage

Create a `.stylelintrc.cjs` file in the root of your project with the following contents. See the [stylelint configuration docs](https://stylelint.io/user-guide/configure) for more details.

```cjs
/** @type {import("stylelint").Config} */
const config = {
	extends: ["@prosomo/stylelint-config"],
};

module.exports = config;
```

Now you can run stylelint by adding a linting script to your `package.json`. See the [stylelint CLI docs](https://stylelint.io/user-guide/usage/cli) for more details.

```json
{
	"scripts": {
		"lint": "stylelint \"src/**/*.css\""
	}
}
```

Run it:

**With pnpm**

```bash
pnpm lint
```

**With Yarn**

```bash
yarn lint
```

**With npm**

```bash
npm run lint
```
