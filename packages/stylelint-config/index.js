/** @type {import("stylelint").Config} */
module.exports = {
	extends: ["stylelint-config-standard", "stylelint-config-prettier"],
	rules: {
		"selector-class-pattern": null,
		"no-descending-specificity": null,
		"declaration-property-value-no-unknown": true,
	},
};
