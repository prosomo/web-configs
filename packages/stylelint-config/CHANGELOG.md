# @prosomo/stylelint-config

## 3.0.0

### Major Changes

- b4adb1a: Upgrade to stylelint-no-unsupported-browser-features@7.0.0, which drops support for old Node versions

### Patch Changes

- Updated dependencies [b4adb1a]
  - @prosomo/browserslist-config@2.0.0

## 2.0.0

### Major Changes

- a157759: Update to stylelint v15

## 1.0.2

### Patch Changes

- 7ee2058: Update dependencies
- Updated dependencies [7ee2058]
  - @prosomo/browserslist-config@1.0.1

## 1.0.1

### Patch Changes

- d26b1b2: Recommend using files for config instead of package.json

## 1.0.0

### Major Changes

- d454cd8: Initial release
