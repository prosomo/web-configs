# `@prosomo/prettier-config`

[![License](https://img.shields.io/gitlab/license/prosomo/web-configs?style=flat-square)](../../LICENSE)
![npm version](https://img.shields.io/npm/v/%40prosomo%2Fprettier-config?style=flat-square)

Shared [Prettier](https://prettier.io) rules for Prosomo.

## Installation

**With pnpm**

```bash
pnpm add --dev prettier @prosomo/prettier-config
```

**With Yarn**

```bash
yarn add --dev prettier @prosomo/prettier-config
```

**With npm**

```bash
npm install --save-dev prettier @prosomo/prettier-config
```

## Usage

Add a `prettier` property in your `package.json`. See the [Prettier configuration docs](https://prettier.io/docs/en/options.html) for more details.

```json
{
	"prettier": "@prosomo/prettier-config"
}
```

Now you can run Prettier by adding linting and formatting scripts to your `package.json`. See the [Prettier CLI docs](https://prettier.io/docs/en/cli.html) for more details.

```json
{
	"scripts": {
		"lint": "prettier --check .",
		"format": "prettier --write ."
	}
}
```

Run it:

**With pnpm**

```bash
pnpm lint
pnpm format
```

**With Yarn**

```bash
yarn lint
yarn format
```

**With npm**

```bash
npm run lint
npm run format
```
