# @prosomo/prettier-config

## 2.0.0

### Major Changes

- b4adb1a: Upgrade to prettier v3, dropping support for v2

## 1.0.3

### Patch Changes

- Remove tabWidth option (useless because we use tabs)

## 1.0.2

### Patch Changes

- d454cd8: Update README

## 1.0.1

### Patch Changes

- Update package metadata and README

## 1.0.0

### Major Changes

- 8ffd336: Initial release
