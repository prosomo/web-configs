module.exports = {
	useTabs: true,
	printWidth: 100,
	endOfLine: "lf",
	trailingComma: "all",
};
