/** @type {import("eslint").ESLint.ConfigData} */
module.exports = {
	extends: ["eslint:recommended", "@prosomo/eslint-config/rules/base", "prettier"],
};
