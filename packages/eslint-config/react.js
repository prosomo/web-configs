/** @type {import("eslint").ESLint.ConfigData} */
module.exports = {
	extends: [
		"eslint:recommended",
		"plugin:react/recommended",
		"plugin:react/jsx-runtime",
		"@prosomo/eslint-config/rules/base",
		"prettier",
	],
	plugins: ["react"],
	settings: { react: { version: "detect" } },
	overrides: [{ files: ["*.js?(x)"] }],
};
