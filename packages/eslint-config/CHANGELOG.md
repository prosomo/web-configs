# @prosomo/eslint-config

## 2.1.1

### Patch Changes

- Adds eslint-plugin-deprecation to peer dependencies

## 2.1.0

### Minor Changes

- Adds `eslint-plugin-deprecation` to warn of deprecated function usage

## 2.0.0

### Major Changes

- b4adb1a: Now requires @typescript-eslint/\* >=6.2.0 and changes the `@typescript-eslint/no-explicit-any` rule to "warn"

## 1.5.0

### Minor Changes

- a157759: Make peer dependencies not optional

## 1.4.0

### Minor Changes

- Update peer dependency requirements

## 1.3.0

### Minor Changes

- a131364: Add the `@typescript-eslint/consistent-type-imports` rule

## 1.2.2

### Patch Changes

- 7ee2058: Update dependencies

## 1.2.1

### Patch Changes

- d26b1b2: Recommend using files for config instead of package.json

## 1.2.0

### Minor Changes

- dc91160: Fix default config and react config file extensions

## 1.1.0

### Minor Changes

- d454cd8: Add optional peer dependencies

## 1.0.2

### Patch Changes

- Update package metadata and README

## 1.0.1

### Patch Changes

- 5538d08: Make all peerDependencies required

## 1.0.0

### Major Changes

- 8ffd336: Initial release
