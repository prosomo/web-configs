# `@prosomo/eslint-config`

[![License](https://img.shields.io/gitlab/license/prosomo/web-configs?style=flat-square)](../../LICENSE)
![npm version](https://img.shields.io/npm/v/%40prosomo%2Feslint-config?style=flat-square)

Shared [ESLint](https://eslint.org) configurations for Prosomo.

## Installation

**With pnpm**

```bash
pnpm add --dev eslint eslint-config-prettier @prosomo/eslint-config
```

**With Yarn**

```bash
yarn add --dev eslint eslint-config-prettier @prosomo/eslint-config
```

**With npm**

```bash
npm install --save-dev eslint eslint-config-prettier @prosomo/eslint-config
```

If you need TypeScript support, also install `typescript`, `@typescript-eslint/eslint-plugin` and `@typescript-eslint/parser`.

If you need React support, also install `eslint-plugin-react`.

## Available configs

- `@prosomo/eslint-config`
- `@prosomo/eslint-config/typescript`
- `@prosomo/eslint-config/react`
- `@prosomo/eslint-config/react-typescript`

## Usage

Create a `.eslintrc.cjs` file in the root of your project with the following contents. See the [ESLint configuration docs](https://eslint.org/docs/latest/user-guide/configuring/configuration-files) for more details.

```cjs
/** @type {import("eslint").ESLint.ConfigData} */
const config = {
	extends: ["@prosomo/eslint-config"],
};

module.exports = config;
```

Now you can run ESLint by adding a linting script to your `package.json`. See the [ESLint CLI docs](https://eslint.org/docs/latest/user-guide/command-line-interface) for more details.

```json
{
	"scripts": {
		"lint": "eslint ."
	}
}
```

Run it:

**With pnpm**

```bash
pnpm lint
```

**With Yarn**

```bash
yarn lint
```

**With npm**

```bash
npm run lint
```
