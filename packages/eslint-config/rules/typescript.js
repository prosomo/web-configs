module.exports = {
	extends: "./base",
	plugins: ["deprecation"],
	rules: {
		"no-unused-vars": "off",
		"deprecation/deprecation": "warn",
		"@typescript-eslint/no-explicit-any": "warn",
		"@typescript-eslint/consistent-type-imports": "error",
		"@typescript-eslint/no-unused-vars": [
			"error",
			{
				argsIgnorePattern: "^_",
				varsIgnorePattern: "^_",
				caughtErrorsIgnorePattern: "^_",
			},
		],
	},
};
