module.exports = {
	env: {
		node: true,
		browser: true,
		es6: true,
	},
	rules: {
		"no-unused-vars": [
			"error",
			{
				argsIgnorePattern: "^_",
				varsIgnorePattern: "^_",
				caughtErrorsIgnorePattern: "^_",
			},
		],
		"default-case": "error",
		"no-use-before-define": "error",
		"no-var": "error",
		"no-case-declarations": "off",
		"no-template-curly-in-string": "error",
		"no-useless-concat": "error",
		"prefer-template": "error",
	},
};
