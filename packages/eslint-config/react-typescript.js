/** @type {import("eslint").ESLint.ConfigData} */
module.exports = {
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:react/recommended",
		"plugin:react/jsx-runtime",
		"@prosomo/eslint-config/rules/typescript",
		"prettier",
	],
	parser: "@typescript-eslint/parser",
	plugins: ["react"],
	settings: { react: { version: "detect" } },
	overrides: [{ files: ["*.{ts,js}?(x)"] }],
};
