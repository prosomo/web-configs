/** @type {import("eslint").ESLint.ConfigData} */
module.exports = {
	parser: "@typescript-eslint/parser",
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/recommended",
		"@prosomo/eslint-config/rules/typescript",
		"prettier",
	],
	overrides: [{ files: ["*.{ts,js}"] }],
};
