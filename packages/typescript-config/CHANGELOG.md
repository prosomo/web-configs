# @prosomo/typescript-config

## 2.2.0

### Minor Changes

- a157759: Make peer dependencies not optional

## 2.1.0

### Minor Changes

- Update peer dependency requirements

## 2.0.0

### Major Changes

- e601b96: - Update to TS 5, which is now required. Lower versions are not compatible.
  - Use `verbatimModuleSyntax` instead of `importsNotUsedAsValues`.

## 1.3.0

### Minor Changes

- d1fc272: Add importNotUsedAsValues

## 1.2.1

### Patch Changes

- 623f1d2: Remove the types field from the configs, as it restricts the global types to only the ones specified. See the documentation: https://www.typescriptlang.org/tsconfig#types

## 1.2.0

### Minor Changes

- d454cd8: Add optional peer dependencies

## 1.1.2

### Patch Changes

- Update package metadata and README

## 1.1.1

### Patch Changes

- a948c8d: Use moduleResolution Node, lib ESNext

## 1.1.0

### Minor Changes

- 6cded86: Add DOM.Iterable to react.json

## 1.0.0

### Major Changes

- 8ffd336: Initial release
