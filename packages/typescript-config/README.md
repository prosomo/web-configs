# `@prosomo/typescript-config`

[![License](https://img.shields.io/gitlab/license/prosomo/web-configs?style=flat-square)](../../LICENSE)
![npm version](https://img.shields.io/npm/v/%40prosomo%2Ftypescript-config?style=flat-square)

Shared extensible [TypeScript](https://www.typescriptlang.org) configurations for Prosomo.

## Available configs

- `@prosomo/typescript-config/base.json`
- `@prosomo/typescript-config/react.json`
- `@prosomo/typescript-config/react-app.json`
- `@prosomo/typescript-config/react-library.json`
- `@prosomo/typescript-config/node.json`
- `@prosomo/typescript-config/node-library.json`

## Installation

**With pnpm**

```bash
pnpm add --dev typescript @prosomo/typescript-config
```

**With Yarn**

```bash
yarn add --dev typescript @prosomo/typescript-config
```

**With npm**

```bash
npm install --save-dev typescript @prosomo/typescript-config
```

If you need Node.js support, also install `@types/node`.

If you need React support, also install `@types/react`.

## Usage

Create a `tsconfig.json` file at the root of your project, and add the desired config to the `extends` parameter. See the [tsconfig Docs](https://typescriptlang.org/tsconfig) for more details.

```json
{
	"extends": "@prosomo/typescript-config/base.json"
}
```

Now you can run TypeScript checks by adding a checking script to your `package.json`. See the [tsc CLI docs](https://www.typescriptlang.org/docs/handbook/compiler-options.html) for more details.

```json
{
	"scripts": {
		"check": "tsc --noEmit"
	}
}
```

Run it:

**With pnpm**

```bash
pnpm check
```

**With Yarn**

```bash
yarn check
```

**With npm**

```bash
npm run check
```

## Examples

### React application

```json
{
	"extends": "@prosomo/typescript-config/react-app.json",
	"compilerOptions": {
		"baseUrl": "./src",
		"rootDir": ".",
		"paths": {
			"$assets": ["assets"],
			"$components": ["components"],
			"$hooks": ["hooks"],
			"$routes": ["routes"],
			"$utils": ["utils"]
		}
	},
	"include": ["./src/**/*"]
}
```

### React library

This example config is assuming the library code sits in the `[project root]/src` folder.

```json
{
	"extends": "@prosomo/typescript-config/react-library.json",
	"compilerOptions": {
		"baseUrl": "./src",
		"rootDir": ".",
		"outDir": "./dist"
	},
	"include": ["./src/**/*"]
}
```

### Other Node projects

This config can be useful for backend servers.

```json
{
	"extends": "@prosomo/typescript-config/node.json",
	"compilerOptions": {
		"baseUrl": "./src",
		"rootDir": ".",
		"outDir": "./dist"
	},
	"ts-node": {
		"transpileOnly": true
	},
	"include": ["./src/**/*"]
}
```
