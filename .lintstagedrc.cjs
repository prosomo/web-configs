module.exports = {
	// Run ESLint on JS files
	"*.js": "eslint --fix",
	// Run Prettier everywhere
	"*": "prettier --write --ignore-unknown",
};
