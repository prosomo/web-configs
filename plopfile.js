const { readdirSync, existsSync } = require("fs");
const { join } = require("path");

function getPackageNames() {
	const packagesPath = join(__dirname, "packages");
	return readdirSync(packagesPath).filter((packageName) => {
		const packageJSONPath = join(packagesPath, packageName, "package.json");
		return existsSync(packageJSONPath);
	});
}

module.exports = function (plop) {
	plop.setGenerator("package", {
		description: "create a new package from scratch",
		prompts: [
			{
				type: "input",
				name: "name",
				message: "What should this package's name be? Ex. eslint-plugin",
			},
			{
				type: "input",
				name: "description",
				message: "What should this package's description be?",
			},
		],
		actions: [
			{
				type: "add",
				path: "packages/{{kebabCase name}}/package.json",
				templateFile: "templates/package.hbs.json",
			},
			{
				type: "add",
				path: "packages/{{kebabCase name}}/README.md",
				templateFile: "templates/README.hbs.md",
			},
			{
				type: "add",
				path: "packages/{{kebabCase name}}/CHANGELOG.md",
				templateFile: "templates/CHANGELOG.hbs.md",
			},
			{
				type: "add",
				path: "packages/{{kebabCase name}}/index.js",
				templateFile: "templates/index.hbs",
			},
		],
	});

	plop.setGenerator("docs", {
		description: "Generate root repo documentation",
		prompts: [],
		actions: [
			{
				type: "add",
				path: "README.md",
				templateFile: "templates/ROOT_README.hbs.md",
				force: true,
				data: { jsPackageNames: getPackageNames() },
			},
		],
	});
};
