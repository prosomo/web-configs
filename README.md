[comment]: # "NOTE: This file is generated and should not be modify directly. Update `templates/ROOT_README.hbs.md` instead"

# Web Configs

[![License](https://img.shields.io/gitlab/license/prosomo/web-configs?style=flat-square)](LICENSE)

This repository contains common configurations for building web apps at Prosomo.

## Usage

This repo is managed as a monorepo that is composed of many npm packages, where each package has its own `README` and documentation describing usage.

### Package Index

| Name                                                | NPM                                                                                             |
| --------------------------------------------------- | ----------------------------------------------------------------------------------------------- |
| [browserslist-config](packages/browserslist-config) | ![npm version](https://img.shields.io/npm/v/%40prosomo%2Fbrowserslist-config?style=flat-square) |
| [eslint-config](packages/eslint-config)             | ![npm version](https://img.shields.io/npm/v/%40prosomo%2Feslint-config?style=flat-square)       |
| [prettier-config](packages/prettier-config)         | ![npm version](https://img.shields.io/npm/v/%40prosomo%2Fprettier-config?style=flat-square)     |
| [stylelint-config](packages/stylelint-config)       | ![npm version](https://img.shields.io/npm/v/%40prosomo%2Fstylelint-config?style=flat-square)    |
| [typescript-config](packages/typescript-config)     | ![npm version](https://img.shields.io/npm/v/%40prosomo%2Ftypescript-config?style=flat-square)   |

## Development

### Getting Started

> Make sure you have enabled `corepack` by running the following `corepack enable`. This makes sure you can use pnpm without manually installing it, and will always use the version specified in the `packageManager` field of `package.json`.

```bash
git clone git@gitlab.com:prosomo/web-configs.git
pnpm i
```

### Creating a new package

Run `pnpm plop package` and follow the prompts.

### Testing your changes in a local project

To try out your changes in another locally cloned project, you can navigate to the project and use `pnpm link <relative-path-to-package>`.

Example: To test my changes to `@prosomo/eslint-config` in my local project named `my-project`, I would run `cd ../path/to/my-project && pnpm link ../path/to/web-configs/packages/eslint-config`.

### Documentation

If your change affects the public API of any packages within this repository (i.e. adding or changing arguments to a function, adding a new function, changing the return value, etc), please ensure the documentation is updated, and a changelog is added to reflect this. Documentation is in the `README.md` files of each package.

## Changesets

Changesets are used in this project to generate our changelogs, automatically version the packages and release them. When making a change, you should **always** generate a changeset describing what has been done. This can be done using `pnpm changeset`, selecting the desired package, the type of version bump that is desired, and finally writing a summary.

Once a package is ready to be released, run `pnpm changeset version`, which will automatically update the package version and its changelog.

To release a package, run `pnpm changeset publish` and follow the instructions. Once that is done, run `git push --follow-tags` to push your changes, with the tags.

Refer to the [changesets documentation](https://github.com/changesets/changesets/blob/main/docs/adding-a-changeset.md#i-am-in-a-multi-package-repository-a-mono-repo) for more info on how to generate changesets.

## License

MIT &copy; [Prosomo](https://prosomo.com/), see [LICENSE](LICENSE) for details.
