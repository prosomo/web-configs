# `@prosomo/{{name}}`

[![License](https://img.shields.io/gitlab/license/prosomo/web-configs?style=flat-square)](../../LICENSE)
![npm version](https://img.shields.io/npm/v/%40prosomo%2F{{name}}?style=flat-square)

{{description}}

## Installation

**With pnpm**

```bash
pnpm add --dev @prosomo/{{name}}
```

**With Yarn**

```bash
yarn add --dev @prosomo/{{name}}
```

**With npm**

```bash
npm install --save-dev @prosomo/{{name}}
```

## Usage
